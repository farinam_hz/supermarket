from django.db import models
from django.contrib.auth.models import User


class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=20)
    address = models.TextField()
    balance = models.PositiveIntegerField(default=20000)

    def serialize(self, customer_id):
        data = {
            "id": customer_id,
            "username": self.user.username,
            "first_name": self.user.first_name,
            "last_name": self.user.last_name,
            "email": self.user.email,
            "phone": self.phone,
            "address": self.address,
            "balance": self.balance
        }
        return data

    def serialize2(self):
        data = {
            "id": self.pk,
            "username": self.user.username,
            "first_name": self.user.first_name,
            "last_name": self.user.last_name,
            "email": self.user.email,
            "phone": self.phone,
            "address": self.address,
            "balance": self.balance
        }
        return data

    def change_first_name(self, first_name):
        self.user.first_name = first_name
        self.save()
        pass

    def change_email(self, email):
        self.user.email = email
        self.save()
        pass

    def change_balance(self, balance):
        self.balance = balance
        self.save()
        pass

    def change_address(self, address):
        self.address = address
        self.save()
        pass

    def deposit(self, amount):
        self.balance += amount
        self.save()
        pass

    def spend(self, amount):
        self.balance -= amount
        if self.balance >= 0:
            self.save()
        else:
            raise Exception("Not Enough Balance")

        pass

