from django.urls import path

from . import views

urlpatterns = [
    # products urls
    path('customer/register/', views.customer_register, name='customer_register'),
    path('customer/list/', views.customer_list, name='customer_list'),
    path('customer/<int:customer_id>/', views.customer, name='customer'),
    path('customer/<int:customer_id>/edit/', views.edit, name='edit'),
    path('customer/login/', views.login_view, name='login'),
    path('login/', views.logout_problem, name='logout_problem'),
    path('customer/logout/', views.logout_view, name='logout'),
    path('customer/profile/', views.profile, name='profile'),
]
