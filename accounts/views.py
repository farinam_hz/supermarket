import json

from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from .models import Customer, User
from django.db.models import Q


def customer_register(request):
    try:
        d = json.loads(request.body.decode(encoding='utf-8'))
        username = d["username"]
        password = d["password"]
        first_name = d["first_name"]
        last_name = d["last_name"]
        email = d["email"]
        phone = d["phone"]
        address = d["address"]
        user = User.objects.create_user(username=username, email=email, password=password,
                                        first_name=first_name, last_name=last_name)
        customer = Customer.objects.create(user=user, phone=phone, address=address)
        customer.save()
    except Exception as e:
        return JsonResponse({"message": str(e)}, status=400)
    return JsonResponse({"id": customer.id}, status=201)
    pass


def customer_list(request):
    if request.GET.get('search', ''):
        search_obj = request.GET.get('search', '')
        c = Customer.objects.filter(
            Q(user__first_name__contains=search_obj) |
            Q(user__last_name__contains=search_obj) |
            Q(user__last_name__contains=search_obj) |
            Q(address__contains=search_obj)
        )
    else:
        c = Customer.objects.all()
    clist = []
    if c is not None:
        for a in c:
            clist.append(Customer.serialize2(a))
    context = {
        'customers': clist
    }
    return JsonResponse(context, status=200)


def customer(request, customer_id):
    try:
        c = Customer.objects.get(pk=customer_id)
    except Customer.DoesNotExist:
        return JsonResponse({"message": "Customer Not Found."}, status=404)
    return JsonResponse(Customer.serialize(c, customer_id), status=200, safe=False)


def edit(request, customer_id):
    try:
        Customer.objects.get(pk=customer_id)
    except Customer.DoesNotExist:
        return JsonResponse({"message": "Customer Not Found."}, status=404)
    try:
        customer = Customer.objects.get(pk=customer_id)
        d = json.loads(request.body.decode(encoding='utf-8'))
        for a in d:
            if a == "username":
                if d["username"] != customer.user.username:
                    return JsonResponse({"message": "Cannot edit customer's identity and credentials."}, status=403)
            elif a == "id":
                if d["id"] != customer.pk:
                    return JsonResponse({"message": "Cannot edit customer's identity and credentials."}, status=403)
            elif a == "password":
                if d["password"] != customer.user.password:
                    return JsonResponse({"message": "Cannot edit customer's identity and credentials."}, status=403)
            elif a == "first_name":
                first_name = d["first_name"]
                Customer.change_first_name(customer, first_name)
            elif a == "email":
                email = d["email"]
                Customer.change_email(customer, email)
            elif a == "balance":
                balance = d["balance"]
                if type(balance) is int:
                    Customer.change_balance(customer, balance)
                else:
                    return JsonResponse({"message": "Balance should be integer."}, status=400)
            elif a == "address":
                address = d["address"]
                Customer.change_address(customer, address)
            else:
                return JsonResponse({"message": "Invalid fields."}, status=400)
    except Exception as e:
        return JsonResponse({"message": str(e)}, status=400)

    return JsonResponse(Customer.serialize(customer, customer_id), status=200, safe=False)


def login_view(request):
    d = json.loads(request.body.decode(encoding='utf-8'))
    username = d["username"]
    password = d["password"]
    user = authenticate(request, username=username, password=password)
    if user is not None:
        # Successful login
        login(request, user)
        context = {
            "message": "You are logged in successfully."
        }
        # cookie send
        return JsonResponse(context, status=200)
    else:
        return JsonResponse({"message": "Username or Password is incorrect."}, status=404)
    pass


@login_required
def logout_view(request):
    if request.user.is_authenticated:
        logout(request)
        return JsonResponse({"message": "You are logged out successfully."}, status=200)
    pass


def profile(request):
    if request.user.is_anonymous:
        return JsonResponse({"message": "You are not logged in."}, status=403)
    else:
        c = request.user.customer
        return JsonResponse(Customer.serialize2(c), status=200)
    pass


def logout_problem(request):
    return JsonResponse({"message": "You are not logged in."}, status=403)
