from django.contrib.auth import models
from django.db import models
from django.contrib.auth.models import User
from django.db import IntegrityError
from accounts.models import Customer


class Product(models.Model):

    code = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=100)
    price = models.PositiveIntegerField('قیمت')
    inventory = models.PositiveIntegerField('موجودی', default=0)

    def serialize2(self):
        data = {
            "id": self.pk,
            "code": self.code,
            "name": self.name,
            "price": self.price,
            "inventory": self.inventory
        }
        return data

    def serialize(self, product_id):
        data = {
            "id": product_id,
            "code": self.code,
            "name": self.name,
            "price": self.price,
            "inventory": self.inventory
        }
        return data

    def increase_inventory(self, amount):
        self.inventory += amount
        self.save()
        pass

    def decrease_inventory(self, amount):
        self.inventory = self.inventory - amount
        if self.inventory >= 0:
            self.save()
        else:
            raise Exception("Not Enough Inventory")

        pass


class OrderRow(models.Model):
    product = models.ForeignKey('Product', on_delete=models.CASCADE)
    order = models.ForeignKey('Order', on_delete=models.CASCADE)
    amount = models.IntegerField


class Order(models.Model):
    # Status values. DO NOT EDIT
    STATUS_SHOPPING = 1
    STATUS_SUBMITTED = 2
    STATUS_CANCELED = 3
    STATUS_SENT = 4
    
    customer = models.ForeignKey('accounts.Customer', on_delete=models.PROTECT)
    order_time = models.DateTimeField
    total_price = models.IntegerField
    status = (
        (STATUS_SHOPPING, 'در حال خرید'),
        (STATUS_SUBMITTED, 'ثبت‌شده'),
        (STATUS_CANCELED, 'لغوشده'),
        (STATUS_SENT, 'ارسال‌شده'),
    )

    @staticmethod
    def initiate(customer):
        if customer.Order.status != Order.STATUS_SHOPPING:
            customer.Order.status = Order.STATUS_SHOPPING
        else:
            raise Exception("You have another order")
        customer.save()
        pass

    def add_product(self, product, amount):
        if (amount > 0) and (amount <= product.inventory):
            my_order = OrderRow(order=self, product=product, amount=amount)
            my_order.save()
        else:
            raise Exception("Not Enough")
        self.save()
        pass

    def remove_product(self, product, amount=None):
        if amount is None:
            OrderRow.objects.get(product=product, order=self, amount=amount).remove()
        else:
            OrderRow.objects.get(product=product, order=self).amount -= amount
        self.save()
        pass

    def submit(self):
        if self.status != Order.STATUS_SHOPPING:
            raise Exception("Not Available")
        else:
            t = 0
            sum_price = 0
            for i in OrderRow.objects.get(order=self):
                sum_price += i.product.price * i.amount
                if i.product.inventory < i.amount:
                    t = 1
                    break
                if sum_price > self.customer.balance:
                    t = 1
                    break
            if t == 1:
                raise Exception("Not Enough")
            if t == 0:
                self.customer.spend(sum_price)
                for i in OrderRow.objects.get(order=self):
                    i.product.decrease_inventory(i.amount)
                self.status = Order.STATUS_SUBMITTED
        self.save()
        pass

    def cancel(self):
        if self.status != Order.STATUS_SUBMITTED:
            raise Exception("Not Available")
        else:
            for i in OrderRow.objects.get(order=self):
                self.customer.deposit(i.amount * i.product.price)
                i.product.increase_inventory(i.amount)
            self.status = Order.STATUS_CANCELED
        self.save()
        pass

    def send(self):
        if self.status != Order.STATUS_SUBMITTED:
            raise Exception("Not Available")
        else:
            self.status = Order.STATUS_SENT
        self.save()
        pass
