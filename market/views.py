"""
    You can define utility functions here if needed
    For example, a function to create a JsonResponse
    with a specified status code or a message, etc.

    DO NOT FORGET to complete url patterns in market/urls.py
"""
import json

from django.core import serializers
from django.http import JsonResponse
from .models import Product


def product_insert(request):

    # hint: you should check request method like below
    if request.method != 'POST':
        return JsonResponse({"message": "Method was not POST"}, status=400)
        pass  # return appropriate error message
    else:
        d = json.loads(request.body.decode(encoding='utf-8'))
        code = d["code"]  # ["code"]
        name = d["name"]  # ["name"]
        price = d["price"]  # ["price"]
        inventory = 0
        if "inventory" in d:
            inventory = d["inventory"]  # ["inventory"]
        try:
            product = Product.objects.create(code=code, name=name, price=price, inventory=inventory)
            product.save()
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)
    return JsonResponse({"id": product.id}, status=201)
    pass  # main logic and return normal response


def product_list(request):
    if request.method != 'GET':
        return JsonResponse({"message": "Method was not GET"}, status=400)
        pass
    else:
        if request.GET.get('search', ''):
            search_obj = request.GET.get('search', '')
            p = Product.objects.filter(name__contains=search_obj)
        else:
            p = Product.objects.all()
        plist = []
        if p is not None:
            for a in p:
                plist.append(Product.serialize2(a))
        context = {
            'products': plist
        }

    return JsonResponse(context, status=200)


def product(request, product_id):
    if request.method != 'GET':
        return JsonResponse({"message": "Method was not GET"}, status=400)
        pass
    else:
        try:
            p = Product.objects.get(pk=product_id)
        except Product.DoesNotExist:
            return JsonResponse({"message": "Product Not Found."}, status=404)

    return JsonResponse(Product.serialize(p, product_id), status=200, safe=False)


def edit_inventory(request, product_id):
    if request.method != 'POST':
        return JsonResponse({"message": "Method was not POST"}, status=400)
        pass
    else:
        try:
            Product.objects.get(pk=product_id)
        except Product.DoesNotExist:
            return JsonResponse({"message": "Product Not Found."}, status=404)
        try:
            product = Product.objects.get(pk=product_id)
            d = json.loads(request.body.decode(encoding='utf-8'))
            amount = d["amount"]
            if amount > 0:
                Product.increase_inventory(product, amount)
            else:
                Product.decrease_inventory(product, amount*-1)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)

    return JsonResponse(Product.serialize(product, product_id), status=200, safe=False)

